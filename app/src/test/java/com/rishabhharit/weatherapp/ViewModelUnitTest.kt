package com.rishabhharit.weatherapp

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import androidx.lifecycle.SavedStateHandle
import com.rishabhharit.weatherapp.model.Currently
import com.rishabhharit.weatherapp.model.Success
import com.rishabhharit.weatherapp.model.UiState
import com.rishabhharit.weatherapp.model.WeatherState
import com.rishabhharit.weatherapp.repository.WeatherRepo
import com.rishabhharit.weatherapp.viewModel.MainActivityVM
import io.mockk.every
import io.mockk.mockk
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import kotlin.random.Random

class ViewModelUnitTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    private lateinit var vm: MainActivityVM

    @Mock
    private lateinit var observer: Observer<UiState<WeatherState>>

    private lateinit var weatherRepo: WeatherRepo

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        RxJavaPlugins.setSingleSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setIoSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setNewThreadSchedulerHandler { Schedulers.trampoline() }

        RxAndroidPlugins.setMainThreadSchedulerHandler { Schedulers.trampoline() }
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }

        weatherRepo = mockk(relaxed = true)
        every { weatherRepo.getWeather(any(), any()) }.returns(Single.just(getWeatherState()))

        val savedStateHandle = mockk<SavedStateHandle>(relaxed = true)
        every { savedStateHandle.get<String>(any()) }.returns(null)

        vm = MainActivityVM(savedStateHandle, weatherRepo)
        vm.curWeatherObservable.observeForever(observer)
    }

    @Test
    fun testWeatherResultSuccess() {
        vm.onLocationFound(Pair(Random(0).nextDouble(), Random(0).nextDouble()))
        assert(vm.curWeatherObservable.value is Success)
        assert((vm.curWeatherObservable.value as Success<WeatherState>).result.currently?.temperature == 65.56)
    }

    private fun getWeatherState() = WeatherState(
        latitude = 59.337239,
        longitude = 18.062381,
        timezone = "Europe/Stockholm",
        currently = Currently(
            time = 1592945079,
            summary = "Clear",
            temperature = 65.56,
            apparentTemperature = 65.56,
            humidity = 0.61,
            windSpeed = 6.25,
            visibility = 10.0,
            pressure = 1023.3
        )
    )


    @After
    fun tearDown() {
        RxAndroidPlugins.reset()
        RxJavaPlugins.reset()
    }
}