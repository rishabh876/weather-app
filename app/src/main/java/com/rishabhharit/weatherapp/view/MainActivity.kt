package com.rishabhharit.weatherapp.view

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Looper
import android.text.format.DateUtils
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.lifecycle.Observer
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.rishabhharit.weatherapp.R
import com.rishabhharit.weatherapp.model.*
import com.rishabhharit.weatherapp.viewModel.MainActivityVM
import com.rishabhharit.weatherapp.viewModel.ViewModelFactory
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val viewModel: MainActivityVM by viewModels { ViewModelFactory(this) }
    private val LOCATION_REQUEST_CODE = 100
    private val REQUEST_CHECK_SETTINGS = 101

    private lateinit var locationClient: FusedLocationProviderClient
    private lateinit var locationRequest: LocationRequest
    private lateinit var locationCallback: LocationCallback
    private lateinit var settingsClient: SettingsClient
    private lateinit var mLocationSettingsRequest: LocationSettingsRequest

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initLocationObjects()

        viewModel.locationRequestObservable.observe(this, Observer { checkPermission() })
        viewModel.curWeatherObservable.observe(this, Observer<UiState<WeatherState>> {
            updateWeather(it)
        })
        viewModel.lastSyncObservable.observe(this, Observer<Long> {
            updateLastSyncTime(it)
        })
        refreshBtn.setOnClickListener { viewModel.refresh() }
    }

    private fun initLocationObjects() {
        locationClient = LocationServices.getFusedLocationProviderClient(this)
        locationRequest = LocationRequest.create().apply {
            priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY
            interval = 20 * 1000
        }
        settingsClient = LocationServices.getSettingsClient(this)
        mLocationSettingsRequest = LocationSettingsRequest.Builder().let {
            it.addLocationRequest(locationRequest)
            it.build()
        }
        initLocationCallback()
    }

    private fun initLocationCallback() {
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                if (locationResult == null) {
                    return
                }
                for (location in locationResult.locations) {
                    if (location != null) {
                        viewModel.onLocationFound(Pair(location.latitude, location.longitude))
                        break
                    }
                }
                locationClient.removeLocationUpdates(locationCallback)
            }
        }
    }

    private fun updateWeather(state: UiState<WeatherState>) {
        when (state) {
            is Loading -> onLoading(state)
            is Failure -> onFailure()
            is Success -> onSuccess(state)
        }
    }

    private fun onFailure() {
        progressGroup.visibility = View.GONE
        postFetchedGroup.visibility = View.VISIBLE
        Toast.makeText(this, getString(R.string.internet_issues), Toast.LENGTH_LONG).show()
    }

    private fun onLoading(state: Loading<WeatherState>) {
        progressGroup.visibility = View.VISIBLE
        if (state.progress == 0) {
            statusTv.text = getString(R.string.getting_location)
        } else {
            statusTv.text = getString(R.string.fetching_weather)
        }
    }

    private fun onSuccess(state: Success<WeatherState>) {
        postFetchedGroup.visibility = View.VISIBLE
        progressGroup.visibility = View.GONE
        state.result.currently?.let {
            tempValueTv.text =
                getString(R.string.temperature, it.temperature?.toString() ?: "-")
            moreInfoResultTv.text =
                "${it.summary}\n${it.apparentTemperature}°F\n${it.humidity}\n${it.visibility}\n${it.windSpeed}"
        }
        tempLabelTv.text = getString(R.string.current_temperature)
    }

    private fun updateLastSyncTime(lastSyncAt: Long) {
        lastSyncTv.text = getString(
            R.string.last_sync,
            DateUtils.getRelativeTimeSpanString(
                System.currentTimeMillis() - lastSyncAt,
                System.currentTimeMillis(),
                DateUtils.SECOND_IN_MILLIS
            )
        )
    }

    private fun checkPermission() {
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
            || ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(this,
                arrayOf(
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ),
                LOCATION_REQUEST_CODE
            )
        } else {
            onPermissionGranted()
        }
    }

    private fun onPermissionGranted() {
        checkLocationSettings()
    }

    @SuppressLint("MissingPermission")
    private fun checkLocationSettings() {
        settingsClient.checkLocationSettings(mLocationSettingsRequest).addOnSuccessListener {
            locationClient.requestLocationUpdates(
                locationRequest,
                locationCallback,
                Looper.getMainLooper()
            )
        }.addOnFailureListener {
            if (it is ResolvableApiException) {
                it.startResolutionForResult(
                    this,
                    REQUEST_CHECK_SETTINGS
                )
            } else
                Toast.makeText(this, "Ensure location is on", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == LOCATION_REQUEST_CODE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                onPermissionGranted()
            } else {
                Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show()
                //todo show have UI retry button
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CHECK_SETTINGS && resultCode == Activity.RESULT_OK) {
            checkLocationSettings()
        }
    }
}