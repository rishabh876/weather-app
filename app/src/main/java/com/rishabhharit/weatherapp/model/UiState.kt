package com.rishabhharit.weatherapp.model

sealed class UiState<T>
class Loading<T>(val progress: Int) : UiState<T>()
class Success<T>(val result: T) : UiState<T>()
class Failure<T>(e: Throwable) : UiState<T>()