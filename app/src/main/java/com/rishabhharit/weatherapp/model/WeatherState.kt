package com.rishabhharit.weatherapp.model

import kotlinx.serialization.Serializable

@Serializable
data class WeatherState(val latitude : Double,
                        val longitude : Double,
                        val timezone : String,
                        val currently : Currently?)

@Serializable
data class Currently (
    val time : Int,
    val summary : String?,
    val temperature : Double?,
    val apparentTemperature : Double?,
    val humidity : Double?,
    val pressure : Double?,
    val windSpeed : Double?,
    val visibility : Double?
)