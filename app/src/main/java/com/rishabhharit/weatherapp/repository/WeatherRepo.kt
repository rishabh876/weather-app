package com.rishabhharit.weatherapp.repository

import com.rishabhharit.weatherapp.model.WeatherState
import com.rishabhharit.weatherapp.utils.Constants
import io.reactivex.Single
import kotlinx.serialization.UnstableDefault
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonConfiguration
import okhttp3.OkHttpClient
import okhttp3.Request

class WeatherRepo(private val client: OkHttpClient) {

    @OptIn(UnstableDefault::class)
    fun getWeather(lat: Double, long: Double): Single<WeatherState> {
        val request: Request = Request.Builder()
            .url("${Constants.WEATHER_URL}/${lat},${long}")
            .build()

        return Single.fromCallable {
            val weatherState = client.newCall(request).execute().body!!.string()
            Json(
                JsonConfiguration(
                    isLenient = true,
                    ignoreUnknownKeys = true,
                    serializeSpecialFloatingPointValues = true,
                    useArrayPolymorphism = true
                )
            ).parse(WeatherState.serializer(), weatherState)
        }
    }
}