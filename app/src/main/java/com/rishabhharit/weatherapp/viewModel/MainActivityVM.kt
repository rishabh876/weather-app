package com.rishabhharit.weatherapp.viewModel

import android.os.Bundle
import androidx.lifecycle.AbstractSavedStateViewModelFactory
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.savedstate.SavedStateRegistryOwner
import com.rishabhharit.weatherapp.model.*
import com.rishabhharit.weatherapp.repository.WeatherRepo
import com.rishabhharit.weatherapp.utils.SingleLiveEvent
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.serialization.UnstableDefault
import kotlinx.serialization.json.Json
import okhttp3.OkHttpClient
import java.util.concurrent.TimeUnit

class MainActivityVM(
    private val state: SavedStateHandle,
    var weatherRepo: WeatherRepo
) :
    ViewModel() {

    //region UI states (LiveData persists during rotation)
    val lastSyncObservable = MutableLiveData(0L)
    var curWeatherObservable = MutableLiveData<UiState<WeatherState>>()
    val locationRequestObservable: SingleLiveEvent<Unit> = SingleLiveEvent()
    //endregion

    private var lastSyncTimeStamp = 0L
    private var lastLocation: Pair<Double, Double>? = null

    private var timerDisposable: Disposable? = null
    private val compositeDisposable = CompositeDisposable()

    // called when activity starts or system kills activity
    init {
        restoreState()
        ensureLatestWeather()
        startTimeElapsedCounter()
    }

    private fun ensureLatestWeather() {
        if (lastLocation == null) {
            findLatestLocation()
        } else if (curWeatherObservable.value !is Success) {
            onLocationFound(lastLocation!!)
        } else {
            startRefreshTimer()
        }
    }

    private fun startTimeElapsedCounter() {
        Observable.interval(5, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .filter { lastSyncTimeStamp > 0 }
            .subscribe { lastSyncObservable.value = System.currentTimeMillis() - lastSyncTimeStamp }
            .let { compositeDisposable.add(it) }
    }

    fun onLocationFound(location: Pair<Double, Double>) {
        lastLocation = location
        state.set(LAT_STATE_KEY, location.first)
        state.set(LONG_STATE_KEY, location.second)

        fetchWeather(location)
    }

    private fun fetchWeather(location: Pair<Double, Double>) {
        curWeatherObservable.value = Loading(1)
        weatherRepo.getWeather(location.first, location.second)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                curWeatherObservable.value = Success(it)
                onPostWeatherFetch(it)
            }, {
                curWeatherObservable.value = Failure(it)
            }).let { compositeDisposable.add(it) }
    }

    @OptIn(UnstableDefault::class)
    private fun onPostWeatherFetch(weather: WeatherState) {
        lastSyncTimeStamp = System.currentTimeMillis()
        lastSyncObservable.value = 0
        state.set(LAST_SYNC_STATE_KEY, System.currentTimeMillis())
        state.set(WEATHER_STATE_KEY, Json.stringify(WeatherState.serializer(), weather))

        startRefreshTimer()
    }

    private fun startRefreshTimer() {
        timerDisposable?.dispose()
        timerDisposable = Observable.interval(
            WEATHER_EXPIRY_DURATION,
            WEATHER_EXPIRY_DURATION,
            TimeUnit.MINUTES
        ).subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { findLatestLocation() }
    }

    private fun findLatestLocation() {
        curWeatherObservable.value = Loading(0)
        locationRequestObservable.value = Unit
    }

    fun refresh() {
        if (lastLocation != null)
            onLocationFound(lastLocation!!)
        else
            findLatestLocation()
    }

    @OptIn(UnstableDefault::class)
    private fun restoreState() {
        state.get<String>(WEATHER_STATE_KEY)?.let { it ->
            Json.parse(WeatherState.serializer(), it).let { weatherState ->
                curWeatherObservable.value = Success(weatherState)
                onPostWeatherFetch(weatherState)
            }
        }
        state.get<Long>(LAST_SYNC_STATE_KEY)?.let {
            lastSyncTimeStamp = it
            lastSyncObservable.value = System.currentTimeMillis() - it
        }
        state.get<Double>(LAT_STATE_KEY)?.let { lat ->
            state.get<Double>(LONG_STATE_KEY)?.let { long ->
                lastLocation = Pair(lat, long)
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        timerDisposable?.dispose()
        compositeDisposable.dispose()
    }

    companion object {
        const val WEATHER_STATE_KEY = "weather"
        const val LAST_SYNC_STATE_KEY = "lastFetchTimeStamp"
        const val LAT_STATE_KEY = "lat"
        const val LONG_STATE_KEY = "long"
        const val WEATHER_EXPIRY_DURATION = 15L
    }
}

class ViewModelFactory(
    owner: SavedStateRegistryOwner,
    defaultArgs: Bundle? = null
) : AbstractSavedStateViewModelFactory(owner, defaultArgs) {
    override fun <T : ViewModel> create(
        key: String,
        modelClass: Class<T>,
        handle: SavedStateHandle
    ): T {
        return MainActivityVM(handle, WeatherRepo(OkHttpClient())) as T
    }
}